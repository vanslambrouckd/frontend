<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('date.timezone', 'Europe/Brussels');

$docRoot = $_SERVER['DOCUMENT_ROOT'];
require($docRoot.'/libs/smarty/libs/Smarty.class.php');
$smarty = new Smarty();

$smarty->setTemplateDir($docRoot.'/templates');
$smarty->setCompileDir($docRoot.'/libs/smarty/templates_c');
$smarty->setCacheDir($docRoot.'/libs/smarty/cache');
// $smarty->setConfigDir('/web/www.example.com/smarty/configs');

$grid_debug = isset($_GET['grid_debug'])?1:0;
$grid_debug = 1;
$smarty->assign('grid_debug', $grid_debug);

$page = isset($_REQUEST['page'])?$_REQUEST['page']:'home';

$file = $docRoot.'/includes/'.$page.'.php';
if (isset($file)) {
	require_once($file);
} else {
	echo 'file niet gevonden';
}
?>