$(document).ready(function() {	
	$('*[data-toggle]').on('click', function(event) {
		event.stopPropagation();
		var el = $(this).attr('data-toggle'); //welk element zichtbaar gemaakt moet worden
		/*
		Aan het menuitem waarop je klikte om een menu te togglen, kan je een klasse togglen
		(bvb om kruisje te tonen indien item geopend)
		*/

		var cls = $(this).attr('data-toggle-class');
		if (cls != undefined) {
			$(this).toggleClass(cls);
		}
		$el = $(el);

		if ($el != undefined) {
			$el.toggleClass('mobile-show');		
		}
	});

	$('body').bind('click touchend', function(event){
		/*
		als je op een element klikt, moet je kijken of het een child element is
		van een parent met class mobile-show (dit is een geopend menuitem / popupmenu)
		indien het een child is van zo een element, doe je niks
		in het andere geval alle popups wegdoen (mobile-show class wegdoen)
		*/
		event.stopPropagation();

		var $target = $(event.target);
		var targetId = $target.attr('id');

		var toggle = $target.attr('data-toggle');
		if (toggle != undefined) {
			return;
		}

		var alleTargets = ['nav-main', 
		'realisaties-overview-filter'
		];

		var $el = $target.closest('.mobile-show');
		if (!$el.hasClass('mobile-show')) {
			$.each(alleTargets, function(i, val) {
				if (val == 'realisaties-overview-filter') {
					$('#realisaties-overview-mobile-toggle').removeClass('open');
				}
				$('#'+val).removeClass('mobile-show');	
			});
		}
	});

	enquire.register("screen and (min-width: 576px)", {
		match: function() {
			$('*[data-equalHeight]').each(function(index) {
				console.log($(this));
				var sel = $(this).attr('data-equalHeight');
				var breakpoint = $(this).attr('data-equalHeight-width');
				if (breakpoint == 576) {
					$(this).find(sel).matchHeight({
					})
				}
			});
		},
		unmatch: function() {
			$('*[data-equalHeight]').each(function(index) {
				console.log($(this));
				var sel = $(this).attr('data-equalHeight');
				var breakpoint = $(this).attr('data-equalHeight-width');
				if (breakpoint == 576) {
					$(this).find(sel).matchHeight({
						remove:true
					})
				}
			});
		}
	});

	function initEqualHeights() {

	}

	$('.js-date').flatpickr({
		disableMobile: "true",
		locale: 'nl',
		dateFormat: 'd/m/Y'
	});
});