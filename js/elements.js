$(document).ready(function() {  
    var sliderOptions = {
        animation: "fade", /* bij slide werkt responsive blijkbaar niet */
        //selector:'.fader-list > li',
        controlNav: false,
        directionNav:true,
        //animationLoop: false,
        //slideshow: true,
        customDirectionNav: $("#test-slider a.navitem"),
        start: function() {
            $('#test-slider .slides').css('visibility', 'visible'); /* flash of unstyled content fix */
        }
    }

    $('#test-slider').flexslider(sliderOptions);
});