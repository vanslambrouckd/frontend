//google.maps.event.addDomListener(window, "load", initializeMap);

$(document).ready(function() {
    /*
    https://codepen.io/Marnoto/pen/xboPmG
    https://snazzymaps.com/help
    API key ingesteld op google account david.vanslambrouck@indiegroup.be
    */

    var styledMapType = new google.maps.StyledMapType(
      [
      {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
        {
          "color": "#444444"
        }
        ]
      },
      {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
        {
          "color": "#f2f2f2"
        }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
        {
          "visibility": "off"
        }
        ]
      },
      {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
        {
          "saturation": -100
        },
        {
          "lightness": 45
        }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
        {
          "visibility": "simplified"
        }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
        {
          "visibility": "off"
        }
        ]
      },
      {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
        {
          "visibility": "off"
        }
        ]
      },
      {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
        {
          "color": "#c5c5c5"
        },
        {
          "visibility": "on"
        }
        ]
      }
      ]
      );

    function initializeMap() {
      var adres = new google.maps.LatLng(51.235102, 3.215087);  
      var marker;
      var map;

      var mapOptions = {
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: adres
      };

      map = new google.maps.Map(document.getElementById("contact-map"),
        mapOptions);

      marker = new google.maps.Marker({
        map:map,
        draggable:true,
        animation: google.maps.Animation.DROP,
        position: adres,
        icon: 'img/layout/googlemaps-marker.png'
      });

      map.mapTypes.set('styled_map', styledMapType);
      map.setMapTypeId('styled_map');

      var infoWindow = new SnazzyInfoWindow({
        marker: marker,
        content: $('#maps-content').html(),
        closeOnMapClick: false
      });


      infoWindow.open();
    }  

    if ($('#contact-map').length > 0) {
      initializeMap();   
    }
  });