<?php
$types_isolatie = array(
    0 => 'Akoestisch',
    1 => 'Thermisch'
);

$smarty->assign('types_isolatie', $types_isolatie);

$breadcrumbs = array('Home', 'Elements');
$smarty->assign('breadcrumbs', $breadcrumbs);

$smarty->display('elements.html');
?>