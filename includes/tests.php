<?php
$pags = array();

$pags[] = array(
    'url' => 'home',
    'title' => '1-Home',
);

$pags[] = array(
    'url' => 'elements',
    'title' => 'Overzicht elementen CSS',
);

$pags[] = array(
    'url' => 'gridtest',
    'title' => 'Gridtest',
);

$smarty->assign('pags', $pags);
$smarty->display('tests.html');
?>